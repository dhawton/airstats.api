<?php
Route::get('/Airport/{id}', 'AirportController@getAirport');
Route::get('/Airport/{id}/Flight/Overview','AirportController@getAirportFlightOverview');
Route::get('/Flights/Live', 'FlightController@getLive');
Route::get('/Flights/{data}','FlightController@getFlights');
Route::get('/Flight/{id}','FlightController@getFlight');
Route::get('/Flight/{id}/{date}/{time}/{dep}/{arr}','FlightController@getFlight');
Route::get('/Flight/{id}/Positions', 'FlightController@getPositions');
Route::get('/Routes/{dep}/{arr}','FlightController@getRoutes');
Route::get('/Statistics/Last/{count}', 'StatisticsController@getLast');
Route::get('/Statistics/General', 'StatisticsController@getGeneral');
Route::get('/Statistics/Top10','StatisticsController@getTop');
Route::get('/Statistics/{id}/Operations','AirportController@getOperations');
Route::get('/Controller/{cid}', function($cid) {
  $user = \App\VatsimName::buildNew($cid);
  echo $user->name;
  return;
});
Route::post('deploy',function() { exec("cd /home/vattrack/vattrack.api && git pull"); });
