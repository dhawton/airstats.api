<?php
namespace App\Classes;

class Calc {
  public static function distanceLatLon($lat1, $lon1, $lat2, $lon2) {
    $degrees = rad2deg(acos((sin(deg2rad($lat1))*sin(deg2rad($lat2))) + (cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($lon1-$lon2)))));

    return $degrees * 59.97662;
  }
}
