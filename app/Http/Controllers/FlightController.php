<?php

namespace App\Http\Controllers;

use App\Classes\Calc;
use Carbon\Carbon;
use App\Flight;
use App\Airport;
use App\VatsimName;
use Illuminate\Http\Request;

class FlightController extends Controller
{
    public function getLive()
    {
        $data = [];
        $flights = Flight::live()->get();
        foreach ($flights as $flight) {
            $dep = Airport::find($flight->departure);
            $arr = Airport::find($flight->arrival);
            $data[] = [
              'id' => $flight->id,
              'callsign' => $flight->callsign,
              'vatsim_id' => $flight->vatsim_id,
              'aircraft_type' => $flight->aircraft_type,
              'dep' => $flight->departure,
              'arr' => $flight->arrival,
              'route' => $flight->route,
              'remarks' => $flight->remarks,
              'lat' => $flight->lat,
              'lon' => $flight->lon,
              'alt' => $flight->alt,
              'hdg' => $flight->hdg,
              'spd' => $flight->spd
            ];
        }

        return response()->json($data, 200);
    }

    public function getFlights($query)
    {
        $data = [];
        $flights = Flight::search($query)->get();
        foreach ($flights as $flight) {
            $user = VatsimName::buildNew($flight->vatsim_id);
            $dep = Airport::find($flight->departure);
            $arr = Airport::find($flight->arrival);
            $dur = 0;
            $diff = null;
            $dept = $arrt = null;
            if ($flight->status === 'Arrived') {
                $dept = new \DateTime($flight->departed_at);
                $arrt = new \DateTime($flight->arrived_at);
                $diff = $dept->diff($arrt);
                $dur = ($diff->d * 24) * 60; // days
              $dur += ($diff->h * 60);     // hours
              $dur += ($diff->i);          // minutes
            }
            $data[] = [
                'id' => $flight->id,
                'callsign' => $flight->callsign,
                'vatsim_id' => $flight->vatsim_id,
                'pilot_name' => $user->name,
                'aircraft_type' => $flight->aircraft_type,
                'dep' => $flight->departure,
                'dep_name' => ($dep) ? $dep->name : "Unknown",
                'arr' => $flight->arrival,
                'arr_name' => ($arr) ? $arr->name : "Unknown",
                'status' => $flight->status,
                'departed_at' => ($flight->departed_at) ? $flight->departed_at->toDateTimeString() : " ",
                'arrived_at' => ($flight->arrived_at) ? $flight->arrived_at->toDateTimeString() : " ",
                'duration' => $dur,
                'duration_h' => ($diff) ? $diff->h : 0,
                'duration_d' => ($diff) ? $diff->d : 0,
                'duration_m' => ($diff) ? $diff->i : 0
            ];
        }

        return response()->json($data, 200);
    }

    public function getFlight($id, $date = null, $time = null, $dep = null, $arr = null)
    {
        $id = str_replace("-", "", $id);
        if (is_numeric($id)) {
            $flight = Flight::find($id);
        } else {
            if (!$date) {
                $flight = Flight::where('callsign', $id)->orderBy('departed_at', 'desc')->first();
            } else {
                $flight = Flight::whereRaw("DATE_FORMAT(`departed_at`, \"%Y%m%d %H%i\")=\"$date $time\"")->where('departure', $dep)->where('arrival', $arr)->where('callsign', $id)->first();
            }
        }
        if (!$flight) {
            return response()->json([], 404);
        }
        $user = VatsimName::buildNew($flight->vatsim_id);
        $dep = Airport::find($flight->departure);
        $arr = Airport::find($flight->arrival);
        $dist_direct = 0;
        $dist_remain = 0;
        if ($dep && $arr) {
            $dist_direct = intval(Calc::distanceLatLon($dep->lat, $dep->lon, $arr->lat, $arr->lon));
            $dist_remain = intval(Calc::distanceLatLon($flight->lat, $flight->lon, $arr->lat, $arr->lon));
        }
        $arrival_est = 0;
        $eet = 0;
        if ($flight->status != "Arrived") {
            if ($flight->spd > 0) {
                $eet = $dist_remain / $flight->spd;
                if ($flight->spd > 250) {
                    $eet += 0.25;    // Add 15 minutes for arrival (estimate)
                }
                $eet = number_format($eet, 3);
                $hour = floor($eet);
                $mins = intval(($eet - $hour) * 60);
                $arrival_est = Carbon::create()->addHours($hour)->addMinutes($mins)->format('H:i');
            } else {
                $arrival_est = "Unknown";
            }
        }

        // Clean up airport locations
        $deparray = [
          'name' => "Unknown",
          'lat' => 0,
          'lon' => 0,
          'city' => "Unknown",
          'admin' => "Unknown",
          'country' => 'ZZ',
          'tz' => "UTC"
        ];
        $arrarray = [
          'name' => "Unknown",
          'lat' => 0,
          'lon' => 0,
          'city' => "Unknown",
          'admin' => "Unknown",
          'country' => 'ZZ',
          'tz' => "UTC"
        ];
        if ($dep) {
            $deparray = [
            'name' => $dep->name,
            'lat' => $dep->lat,
            'lon' => $dep->lon,
            'city' => $dep->city,
            'admin' => ((strlen($dep->admin) > 2) ? str_replace('County ', '', ucwords($dep->admin)) : $dep->admin),
            'country' => $dep->country_id,
            'tz' => $dep->timezone
          ];
        }
        if ($arr) {
            $arrarray = [
            'name' => $arr->name,
            'lat' => $arr->lat,
            'lon' => $arr->lon,
            'city' => $arr->city,
            'admin' => ((strlen($arr->admin) > 2) ? str_replace('County ', '', ucwords($arr->admin)) : $arr->admin),
            'country' => $arr->country_id,
            'tz' => $arr->timezone
          ];
        }

        $dur = 0;
        $diff = null;
        $dept = $arrt = null;
        if ($flight->status === 'Arrived') {
            $dur = $flight->departed_at->diffInMinutes($flight->arrived_at);
        }
        $data = [
            'id' => $flight->id,
            'callsign' => $flight->callsign,
            'vatsim_id' => $flight->vatsim_id,
            'pilot_name' => $user->name,
            'aircraft_type' => $flight->aircraft_type,
            'dep' => $flight->departure,
            'dep_name' => $deparray['name'],
            'dep_lat' => $deparray['lat'],
            'dep_lon' => $deparray['lon'],
            'dep_city' => $deparray['city'],
            'dep_admin' => $deparray['admin'],
            'dep_country' => $deparray['country'],
            'dep_tz' => $deparray['tz'],
            'arr' => $flight->arrival,
            'arr_name' => $arrarray['name'],
            'arr_lat' => $arrarray['lat'],
            'arr_lon' => $arrarray['lon'],
            'arr_city' => $arrarray['city'],
            'arr_admin' => $arrarray['admin'],
            'arr_country' => $arrarray['country'],
            'arr_tz' => $arrarray['tz'],
            'status' => $flight->status,
            'route' => $flight->route,
            'remarks' => $flight->remarks,
            'req_alt' => $flight->planned_alt,
            'lat' => $flight->lat,
            'lon' => $flight->lon,
            'hdg' => $flight->hdg,
            'spd' => $flight->spd,
            'alt' => $flight->alt,
            'departed_at' => ($flight->departed_at) ? $flight->departed_at->toDateTimeString() : " ",
            'arrived_at' => ($flight->arrived_at) ? $flight->arrived_at->toDateTimeString() : " ",
            'dist_direct' => round($dist_direct, 1),
            'dist_remain' => round($dist_remain, 1),
            'arrival_est' => $arrival_est,
            'duration' => $dur,
            'duration_h' => ($diff) ? $diff->h : 0,
            'duration_d' => ($diff) ? $diff->d : 0,
            'duration_m' => ($diff) ? $diff->i : 0
        ];

        return response()->json($data);
    }

    public function getPositions($id)
    {
        $flight = Flight::find($id);
        if (!$flight) {
            return response()->json([], 404);
        }

        if ($flight->archived) {
            $data = json_decode(bzdecompress(
                file_get_contents("https://airstats.blob.core.windows.net/" . $flight->archived)
            ));
        } else {
            $data = [];
            foreach ($flight->positions()->get() as $position) {
                $data[] = [
                    'id' => $position->id,
                    'lat' => $position->lat,
                    'lon' => $position->lon,
                    'alt' => $position->alt,
                    'spd' => $position->spd,
                    'hdg' => $position->hdg,
                    'date' => $position->created_at
                ];
            }
        }
        return response()->json($data);
    }

    public function getRoutes($dep, $arr)
    {
        $routes = Flight::where('created_at', '>=', Carbon::now()->subDays(7))->where('status', 'NOT LIKE', 'Departing Soon')->where('departure', $dep)->where('arrival', $arr)->orderBy('created_at', 'desc')->get();

        $data = [];
        foreach ($routes as $route) {
            $data[] = [
                'flight_id' => $route->id,
                'callsign' => $route->callsign,
                'dep' => $route->departure,
                'arr' => $route->arrival,
                'route' => $route->route,
                'altitude' => $route->planned_alt,
                'aircraft_type' => $route->aircraft_type,
                'departed_at' => ($route->departed_at) ? $route->departed_at : " "
            ];
        }

        return response()->json($data);
    }
}
