<?php

namespace App\Http\Controllers;

use App\Airport;
use App\Flight;
use Illuminate\Http\Request;

class AirportController extends Controller
{
    public function getAirportFlightOverview(Request $request, $id) {
        $id = strtoupper($id);
        $apt = Airport::find($id);
        if (!$apt) {
            return response()->json([], 404);
        }
        if (\Cache::has("apt.$id.traffic")) {
            return response()->json(\Cache::get("apt.$id.traffic"), 200, [], JSON_NUMERIC_CHECK);
        }

        $data = [];
        $data['arrivals'] = [];
        $data['departures'] = [];
        $data['enroute'] = [];
        $data['scheduled'] = [];

        foreach (Flight::where('arrival', $id)->where('status', 'En-Route')->orderBy('arrival_est','DESC')->limit(10)->get() as $flight) {
            $depap = Airport::find($flight->departure);
            $data['enroute'][] = [
                'id' => $flight->id,
                'callsign' => $flight->callsign,
                'aircraft_type' => $flight->aircraft_type,
                'departure' => (($depap) ? $depap->name : "") . " ($depap)",
                'departed_at' => $flight->departed_at,
                'arrival_est' => $flight->arrival_est
            ];
        }

        foreach (Flight::where('arrival', $id)->where('status', 'Arrived')->orderBy('arrived_at','DESC')->limit(10)->get() as $flight) {
            $depap = Airport::find($flight->departure);
            $data['arrivals'][] = [
                'id' => $flight->id,
                'callsign' => $flight->callsign,
                'aircraft_type' => $flight->aircraft_type,
                'departure' => (($depap) ? $depap->name : "") . " ($depap)",
                'departed_at' => $flight->departed_at,
                'arrival_est' => $flight->arrival_est
            ];
        }

        foreach (Flight::where('departure', $id)->where('status', 'En-Route')->orderBy('departed_at','DESC')->limit(10)->get() as $flight) {
            $depap = Airport::find($flight->departure);
            $arrap = Airport::find($flight->arrival);
            $data['departures'][] = [
                'id' => $flight->id,
                'callsign' => $flight->callsign,
                'aircraft_type' => $flight->aircraft_type,
                'departure' => (($depap) ? $depap->name : "") . " ($depap)",
                'arrival' => (($arrap) ? $arrap->name : "") . " ($arrap)",
                'departed_at' => $flight->departed_at,
                'arrival_est' => $flight->arrival_est
            ];
        }

        foreach (Flight::where('departure', $id)->where('status', 'Departing Soon')->orderBy('created_at','ASC')->limit(10)->get() as $flight) {
            $depap = Airport::find($flight->departure);
            $arrap = Airport::find($flight->arrival);
            $data['scheduled'][] = [
                'id' => $flight->id,
                'callsign' => $flight->callsign,
                'aircraft_type' => $flight->aircraft_type,
                'departure' => (($depap) ? $depap->name : "") . " ($depap)",
                'arrival' => (($arrap) ? $arrap->name : "") . " ($arrap)",
            ];
        }

        \Cache::put("apt.$id.traffic", $data, 5);
        return response()->json($data, 200, [], JSON_NUMERIC_CHECK);
    }

    public function getAirport(Request $request, $id) {
        $id = strtoupper($id);
        $apt = Airport::find($id);
        if (!$apt) {
            return response()->json([], 404);
        }
        if (\Cache::has("apt.$id")) {
            return response()->json(\Cache::get("apt.$id"), 200, [], JSON_NUMERIC_CHECK);
        }

        $data = [];
        $metar = file_get_contents("http://tgftp.nws.noaa.gov/data/observations/metar/stations/$id.TXT");
        if (!$metar) {
            $metar = "$id METAR NOT AVAILABLE";
        } else {
            $metar = preg_replace("/^\d+\/\d+\/\d+ \d\d:\d\d\n/", '', $metar);
        }
        $taf = file_get_contents("http://tgftp.nws.noaa.gov/data/forecasts/taf/stations/$id.TXT");
        if (!$taf) {
            $taf = "$id TAF NOT AVAILABLE/NOT PRODUCED";
        } else {
            $taf = preg_replace("/^\d+\/\d+\/\d+ \d\d:\d\d\n/", '', $taf);
        }
        $data['id'] = $id;
        $data['name'] = $apt->name;
        $data['city'] = $apt->city;
        $data['admin'] = $apt->admin;
        $data['country'] = $apt->country_id;
        $data['timezone'] = $apt->timezone;
        $data['location'] = [round($apt->lat, 6), round($apt->lon, 6)];
        $data['elevation'] = $apt->elevation;
        $data['metar'] = $metar;
        $data['taf'] = $taf;

        \Cache::put("apt.$id", $data, 10);
        return response()->json($data, 200, [], JSON_NUMERIC_CHECK);
    }

    public function getOperations($id)
    {
        $apt = Airport::find($id);
        if (!$apt) {
            return response()->json([], 404);
        }

        if (\Cache::has("ops.$id")) {
            return response()->json(\Cache::get("ops.$id"));
        }

        $data = [];
        $deps = \DB::select("SELECT DATE(`created_at`) AS `date`, COUNT(`id`) AS `number` FROM `flights` WHERE `created_at` >= date_sub(NOW(), INTERVAL 1 YEAR) AND `departure`='$id' GROUP BY DATE(`created_at`) ORDER BY `created_at`");
        $last_date = null;
        foreach ($deps as $dep) {
            $d = $dep->date;
            $c = $dep->number;
            $data['departures'][$d] = $c;
        }
        // Fill in gaps
        // get start and end out of array
        reset($data['depatures']);
        $start = new \DateTime(key($data['departures']));

        end($data['departures']);
        $end = new \DateTime(key($data['departures']));

        foreach (new \DatePeriod($start, new \DateInterval('P1D'), $end) as $date) {
            $dateKey = $date->format('Y-m-d'); // get properly formatted date out of DateTime object
            if (!isset($data['departures'][$dateKey])) {
                $data['departures'][$dateKey] = 0;
            }
        }

        $arrs = \DB::select("SELECT DATE(`created_at`) AS `date`, COUNT(`id`) AS `number` FROM `flights` WHERE `created_at` >= date_sub(NOW(), INTERVAL 1 YEAR) AND `arrival`='$id' GROUP BY DATE(`created_at`) ORDER BY `created_at`");
        foreach ($arrs as $arr) {
            $d = $arr->date;
            $c = $arr->number;
            $data['arrivals'][$d] = $c;
        }

        // Fill in gaps
        // get start and end out of array
        reset($data['arrivals']);
        $start = new \DateTime(key($data['arrivals']));

        end($data['arrivals']);
        $end = new \DateTime(key($data['arrivals']));

        foreach (new \DatePeriod($start, new \DateInterval('P1D'), $end) as $date) {
            $dateKey = $date->format('Y-m-d'); // get properly formatted date out of DateTime object
            if (!isset($data['arrivals'][$dateKey])) {
                $data['arrivals'][$dateKey] = 0;
            }
        }

        \Cache::put("ops.$id", $data, 60 * 12); // Cache for 12 hours
        return response()->json($data);
    }
}
