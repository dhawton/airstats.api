<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 5/2/2017
 * Time: 10:34 AM
 */

namespace App\Http\Controllers;
use App\Flight;
use App\Airport;

class StatisticsController extends Controller
{
    public function getLast($count) {
        if ($count > 10) {
            $count = 10;
        }

        $flights = Flight::where('status', 'Arrived')->orderBy('updated_at', 'DESC')->limit($count)->get();
        $data = [];
        foreach($flights as $flight) {
          $dep = Airport::find($flight->departure);
          $arr = Airport::find($flight->arrival);
            $data[] = [
                'id' => $flight->id,
                'callsign' => $flight->callsign,
                'dep' => $flight->departure,
                'arr' => $flight->arrival,
                'depname' => ($dep) ? $dep->name : "Unknown",
                'arrname' => ($arr) ? $arr->name : "Unknown",
                'created_at' => $flight->created_at,
                'updated_at' => $flight->updated_at
            ];
        }

        return response()->json($data);
    }

    public function getGeneral() {
      if (\Cache::has('stats.general')) {
        return response()->json(\Cache::get('stats.general'));
      }
      $enroute = \DB::table("flights")->where("status", "En-Route")->count();
      $departing = \DB::table("flights")->where("status","Departing Soon")->count();
      $arrived = \DB::table("flights")->where("status","Arrived")->count();
      $incomplete = \DB::table("flights")->where("status","Incomplete")->count();
      $total = $enroute + $departing + $arrived + $incomplete;

      $data = [
        'total' => $total,
        'departing' => $departing,
        'enroute' => $enroute
      ];
      \Cache::put('stats.general', $data, 10);
      return response()->json($data);
    }

    public function getTop() {
      if (\Cache::has('stats.top10')) {
        return response()->json(\Cache::get('stats.top10'));
      }
      $data = [];
      $dep10 = \DB::select("SELECT `departure`, COUNT(`id`) AS `number` FROM `flights` WHERE `created_at` >= date_sub(NOW(), INTERVAL 7 DAY) GROUP BY `departure` ORDER BY `number` DESC LIMIT 10");
      foreach ($dep10 as $dep) {
        $data['departures'][] = [
          'icao' => $dep->departure,
          'count' => $dep->number
        ];
      }
      $arr10 = \DB::select("SELECT `arrival`, COUNT(`id`) AS `number` FROM `flights` WHERE `created_at` >= date_sub(NOW(), INTERVAL 7 DAY) GROUP BY `arrival` ORDER BY `number` DESC LIMIT 10");
      foreach ($arr10 as $arr) {
        $data['arrivals'][] = [
          'icao' => $arr->arrival,
          'count' => $arr->number
        ];
      }

      $data['updated'] = date("Y-m-d H:i:s");

      \Cache::put('stats.top10', $data, 60);
      return response()->json($data);
    }
}
