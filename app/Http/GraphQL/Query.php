<?php
namespace App\Http\GraphQL;

class Query
{
    public function airport($root, array $args, $context, $info) {
        return \App\Airport::find($args['id']);
    }

    public function flight($root, array $args, $context, $info) {
        return \App\Flight::find($args['id']);
    }

    public function flights($root, array $args, $context, $info) {
        return \App\Flight::where("vatsim_id", $args['data'])->orWhere("callsign", $args['data'])->get();
    }
}
