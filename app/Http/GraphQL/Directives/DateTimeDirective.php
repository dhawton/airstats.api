<?php
namespace App\Http\GraphQL\Directives;

use Carbon\Carbon;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Support\Contracts\FieldResolver;
use Nuwave\Lighthouse\Support\Traits\HandlesDirectives;

class DateTimeDirective implements FieldResolver
{
    use HandlesDirectives;

    /**
     * Name of the directive.
     *
     * @return string
     */
    public function name()
    {
        return 'dateTime';
    }

    /**
     * Resolve the field directive.
     *
     * @param FieldValue $value
     *
     * @return FieldValue
     */
    public function resolveField(FieldValue $value)
    {
        $field = $value->getFieldName();

        return $value->setResolver(function ($root) use ($field) {
            $carbon = data_get($root, $field);

            // Format this however you want, but make sure it's a string
            return $carbon instanceof Carbon ? $carbon->toIso8601String() : $carbon;
        });
    }
}
