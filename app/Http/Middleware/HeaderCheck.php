<?php

namespace App\Http\Middleware;

use Closure;

class HeaderCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (!empty($request->header("24map"))) {
        return $next($request);
      }

      if ($request->path() == "graphql") return $next($request);

        if (!$request->header("origin", null)) {
          return response()->json(["status" => "Access Denied"]);
        }

        $host = parse_url($request->header("origin"), PHP_URL_HOST);
        if (!in_array($host, ["localhost","www.airstats.org"])) {
          return response()->json(["status" => "Access Denied"]);
        }

        return $next($request);
    }
}
