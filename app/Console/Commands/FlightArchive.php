<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Position;
use App\Flight;
use Illuminate\Console\Command;

class FlightArchive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flight:archive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Archive flight positions to S3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $flights = Flight::where('archived', '')->where(function ($query) {
            $query->where('status', 'Arrived')->orWhere('status', 'Incomplete');
        })->where('updated_at', "<=", Carbon::yesterday())->limit(1000)->get();
        foreach($flights as $flight) {
          $posdata = [];
          $positions = Position::where('flight_id',$flight->id)->orderBy('created_at','ASC')->get();
          foreach($positions as $position) {
            $posdata[] = [
              'id' => $position->id,
              'lat' => $position->lat,
              'lon' => $position->lon,
              'alt' => $position->alt,
              'spd' => $position->spd,
              'hdg' => $position->hdg,
              'date' => $position->created_at
            ];
            $position->delete();
          }
          $filename = "flights/" . $flight->callsign . "_" . $flight->created_at->format('YmdHis') . ".json";
          \Storage::disk('s3')->put($filename, bzcompress(json_encode($posdata, JSON_NUMERIC_CHECK), 9), 'public');
          $flight->archived = $filename;
          $flight->save();
        }
    }
}
