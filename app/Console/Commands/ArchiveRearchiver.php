<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Flight;

class ArchiveRearchiver extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flight:rearchive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $flights = Flight::where('archived', 'NOT LIKE', '')->get();
        foreach ($flights as $flight) {
          $data = \Storage::disk('s3')->get($flight->archived);
          \Storage::disk('s3')->put($flight->archived, bzcompress($data, 9));
        }
    }
}
