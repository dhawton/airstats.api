<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VatsimName extends Model
{
    protected $table = "vatsim_names";
    public $incrementing = false;
    protected $primaryKey = "cid";
    public $timestamps = false;

    public function GetIdAttribute() {
        return $this->cid;
    }

    public static function buildNew($cid) {
      $user = VatsimName::find($cid);
      if (!$user) {
        $xml = new \SimpleXMLElement(file_get_contents("https://cert.vatsim.net/vatsimnet/idstatusint.php?cid=$cid"));
        $lname = $xml->user->{'name_last'};
        $fname = $xml->user->{'name_first'};
        $user = new VatsimName();
        $user->cid = $cid;
        $user->name = $fname . " " . $lname;
        $user->save();
        return $user;
      } else {
        return $user;
      }
    }
}
