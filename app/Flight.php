<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $table = "flights";
    protected $dates = ['created_at','updated_at','arrived_at','departed_at','arrival_est'];

    public static function live() {
      return Flight::where('status', 'NOT LIKE', 'Arrived')->where('status', 'NOT LIKE', 'Incomplete');
    }

    public static function search($data) {
        return Flight::where('vatsim_id',$data)->orWhere('callsign',$data)->orderBy("created_at","DESC");
    }

    public function GetPositionsAttribute() {
        if ($this->archived) {
            $data = json_decode(bzdecompress(
                file_get_contents("https://airstats.blob.core.windows.net/" . $this->archived)
            ));

            \Log::info(json_encode($data));
            return $data;
        } else {
            return $this->hasMany('App\Position', 'flight_id', 'id')->orderBy("created_at", "ASC");
        }
    }

    public function positions() {
        \Log::info(json_encode($this));
        if ($this->archived) {
            $data = json_decode(bzdecompress(
                file_get_contents("https://airstats.blob.core.windows.net/" . $this->archived)
            ));

            \Log::info(json_encode($data));
            return $data;
        } else {
            return $this->hasMany('App\Position', 'flight_id', 'id')->orderBy("created_at", "ASC");
        }
    }

    public function departureApt() {
        if ($this->departure == null) return null;

        return $this->hasOne("App\Airport", "id", "departure");
    }
    public function arrivalApt() {
        if ($this->arrival == null) return null;

        return $this->hasOne("App\Airport", "id", "arrival");
    }

    public function user() {
        return $this->hasOne("App\VatsimName", "cid", "vatsim_id");
    }
}
